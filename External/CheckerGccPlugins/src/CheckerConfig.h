// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file CheckerGccPlugins/src/CheckerConfig.h
 * @author scott snyder <snyder@bnl.gov>
 * @date May, 2022
 * @brief Very simple config file reader.
 */


#ifndef CHECKERGCCPLUGINS_CHECKERCONFIG_H
#define CHECKERGCCPLUGINS_CHECKERCONFIG_H


#include <vector>
#include <string>
#include <iostream>
#include <unordered_map>


namespace CheckerGccPlugins {


/**
 * @brief Very simple config file reader.
 *
 * This class manages configuration information for the plugins.
 * It may be read from a file, which consists of lines containing key names
 * in square brackets followed by a set of strings, one per line.
 * Any text after a # character is discarded, as are leading/trailing
 * spaces.  Blank lines are ignored.  Example:
 *
 *@code
 [key1]
 This is one line for key1.
 This is another line for key1.  # a comment

 [key2]
 And for key2.
 @endcode
 *
 * We can read files given a path or an environment variable containing
 * a colon-separated list of paths.  If the same key is present in multiple
 * files, the values are concatencated.
 */
class CheckerConfig
{
public:
  /**
   * @brief Default constructor.
   *
   * The configuration will be empty.
   */
  CheckerConfig() = default;


  /**
   * @brief Construct configuration from files given in an environment variable.
   * @param envvar Name of the environment variable to read.
   *
   * @c envvar should expand to a colon-separated list of configuration
   * files to read.
   */
  CheckerConfig (const std::string& envvar);


  /**
   * @brief Look up a configuration entry.
   * @param key The key to look up.
   *
   * Returns an empty vector if the key is not present in the configuration.
   */
  const std::vector<std::string>& operator[] (const std::string& key) const;


  /**
   * @brief Read a configuration file from a C++ stream.
   * @param s The stream from which to read.
   */
  void read (std::istream& s);


  /**
   * @brief Read a configuration file given a pathname.
   * @param path The name of the file to read.
   */
  void read (const std::string& path);


  /**
   * @brief Read configuration files given by an environment variable.
   * @param envvar Name of the environment variable to read.
   *
   * @c envvar should expand to a colon-separated list of configuration
   * files to read.
   */
  void readViaEnv (const std::string& envvar);


  /**
   * @brief Dump the configuration.
   * @param s Stream to which to write.
   */
  void dump (std::ostream& s) const;

private:
  /**
   * @brief Add a new list of strings for a key.
   * @param key Key to which to add.
   * @param data List of strings to append to the list for this key.
   */
  void newkey (std::string&& key, std::vector<std::string>&& data);


  /// The configuration information.
  std::unordered_map<std::string, std::vector<std::string> > m_map;
};


} // namespace CheckerGccPlugins


#endif // not CHECKERGCCPLUGINS_CHECKERCONFIG_H
